[AWS CLI](https://aws.amazon.com/cli/)
----

The [AWS CLI](https://aws.amazon.com/cli/) is a command line set of utilities that allows you to work with AWS from your terminal.

While you may not use the AWS CLI on a regular basis, the [Maven](https://maven.apache.org) build utility requires the CLI do manage the Amazon DynamoDB
instance, whether or not you choose to run DynamoDB locally or in AWS. As with many of prerequisites, the AWS CLI can be installed via Homebrew:

```
	brew install awscli
```

The AWS CLI provides a bash completion facility that can be installed and configured via your *.bash_profile* or *.bashrc* file. Place the following in one of the aforementioned files

```
	complete -C aws_completer aws
```

After starting a new terminal or terminal window, *tab complete* should now function with the AWS CLI. Please see further documentation for configuring the AWS environment.

Note, the AWS CLI requires user credentials **regardless** of whether or not you are running DynamoDB locally or in the AWS Cloud environment.

If you do not have a AWS account, email <sysops@webpt.com> to request access. Once provided, you will need to [configure your AWS CLI environment](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html).

Once configured, a simple test will confirm if your settings are correct

```
	aws dynamodb list-tables
```

A list of the existing tables in the AWS Cloud should be printed to your console as a JSON object:

```
{
    "TableNames": [
        "MusicCollection",
        "Patient",
        "Pin",
        "PinSimple",
        "Questionnaire",
        "TestTable123"
    ]
}

```

[Apache Maven](https://maven.apache.org)
----

The Apache Maven build utility serves as a project compilation, build and depedency management tool serving the role of both [Phing](https://www.phing.info) and [Composer](https://getcomposer.org) for Java development. The details of [Maven](https://maven.apache.org) are explained in other documents, for our purposes installation will suffice. The easiest way to install Maven is via [Homebrew](#Homebrew) as explained above:

```
brew install maven
```
The *mvn* utility should now be in ```/usr/local/bin``` (the default Homebrew installation location) and ready for use.


[Amazon DynamoDB](http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html)
----

Amazon's DynamoDB is a NoSQL, *key-value* based data store allowing for the persistence of unstructured data. By default, DynamoDB is provided as a part of the [Amazon Web Services](https://aws.amazon.com) infrastructure. While the Lazurus Service ultimately uses DynamoDB in AWS, it is not required for development as a *local* DynamoDB instance can be used with very few modifications. Please see the [documentation](./README-DYNAMODB.md) for running DynamoDB locally for an explanation of the differences between a local development DynamoDB instance versus access DynamoDB in the AWS environment.

####DynamoDB via Homebrew

As with [Apache Maven](https://maven.apache.org) and the https://aws.amazon.com/cli/, DynamoDB can be installed via Homebrew

```
brew install dynamodb-local
```

The Homebrew installation method allows for running DynamoDB Local as a [launchd](http://launchd.info) service which can be configured via

```
brew services start dynamodb-local
```

Launchd is generally controlled via the [launchctl](https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man1/launchctl.1.html) utility which can be a bit unweildy for the uninitiated. Fortunately, the [lunchy](https://github.com/eddiezane/lunchy) wrapper removes a great deal of the complexity and can be easily installed via the [Ruby](http://www.ruby-lang.org/en/) gem manager

```
gem install lunchy
```

Please refer to the [lunhcy](https://github.com/eddiezane/lunchy) documentation for further configuration options as well as how to use the wrapper to start, stop DynamoDB.


####DynamoDB via Tarball/Zip

As an alternative to Homebrew, DynamoDB local can be [downloaded](http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html#DynamoDBLocal.DownloadingAndRunning) and installed locally. Please following the instructions referenced in the link above for installing DynamoDB in this manner.

Dynamo Tips and Tricks
----
- By default, DynamoDB Local runs on port 8000
- DynamoDB Local provides a [shell UI](http://localhost:8000/shell)



DynamoDB local comes packaged as a Java jar file and as such can be easily started and used without complicated installation and configuration. Simply unzip the tarball or zip file, this creates the *dynamodb_local_<release-date> directory. Once this is done, you can simply execute

```
java -Djava.library.path=$DYNAMO_HOME/DynamoDBLocal_lib -jar $DYNAMO_HOME/DynamoDBLocal.jar -sharedDb &> /tmp/dynamo.log &
```

where $DYNAMO_HOME is the path to your DynamoDB installation root. Similarly, you can place the above the command in your *.bash_profile* or *.bashrc* file as an alias.

Ideally, you would like a way to stop, start and query the status of DynamoDB as locally it runs as a background task or takes up a console window if you run it in the forground. //TODO -- bash scripts
