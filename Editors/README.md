# Editors

The [Editor Wars](https://en.wikipedia.org/wiki/Editor_war) was originally about vim vs. emacs.

Now there are multiple tools promising to delivery simplicity, value and efficency to your workflow.

Here is a suggested list of editors to use to be successful at WebPT.

Want to see more [editors choices](https://en.wikipedia.org/wiki/Editor_war)
