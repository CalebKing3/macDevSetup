# Homebrew

Package managers make it so much easier to install and update applications (for Operating Systems) or libraries (for programming languages).

### Install

An important dependency before Homebrew can work is the **Command Line Tools** for **Xcode**. These include compilers that will allow you to build things from source.

Open a new terminal, then go to [Install Homebrew](http://brew.sh/) and follow the installation instructions

One thing we need to do is tell the system to use programs installed by Hombrew (in `/usr/local/bin`) rather than the OS default if it exists. We do this by adding `/usr/local/bin` to your `$PATH` environment variable:

    $ echo 'export PATH="/usr/local/bin:$PATH"' >> ~/.bash_profile
    ## OR if you are using zsh
    $ echo 'export PATH="/usr/local/bin:$PATH"' >> ~/.zshrc
