# Git and [Gitlab](https://docs.gitlab.com/ce/development/architecture.html)

What's a developer without [Git](http://git-scm.com/)? To install, simply run:

    $ brew install git

When done, to test that it installed fine you can run:

    $ git --version

And `$ which git` should output `/usr/local/bin/git`.

Next, we'll define your Git user (should be the same name and email you use for [Gitlab](https://gitlab.webpt.com/):

    $ git config --global user.name "Your Name Here"
    $ git config --global user.email "your_email@youremail.com"

They will get added to your `.gitconfig` file.


- - -

### SSH Config for Gitlab
Setting up SSH is really simple as well. Most of the instructions below are referenced from [here](https://docs.gitlab.com/ce/ssh/README.html).

- - -

### DS_Store
On a Mac, it is important to remember to add `.DS_Store` (a hidden OS X system file that's put in folders) to your `.gitignore` files.

- - -

### SourceTree
If you are not a terminal wizard yet. We suggest you use [SourceTree](https://confluence.atlassian.com/bitbucket/set-up-sourcetree-603488472.html)
It is a great [VCS](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control) that makes merges, rebases, committing, switching branch, viewing history, etc. much easier.

[Install](https://www.sourcetreeapp.com/)
Open the application and go to **SourceTree > Install Command Line Tools**

    $ cd ~/YOURPROJECT
    ## Within your project run the following command
    $ stree


If you run into issues trying to install command line tools then try running the following cmd next.

	$ ln -s /Applications/SourceTree.app/Contents/Resources/stree /usr/local/bin/

- - -
