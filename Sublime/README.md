## Sublime Text

With the terminal, the text editor is a developer's most important tool. Everyone has their preferences, but a lot of people are going to tell you that [Sublime Text](http://www.sublimetext.com/) is very popular amongst the community.

Go ahead and [download](http://www.sublimetext.com/) it. Open the **.dmg** file, drag-and-drop in the **Applications** folder, you know the drill now. Launch the application.

**Note**: At this point I'm going to create a shorcut on the OS X Dock for both for Sublime Text and iTerm. To do so, right-click on the running application and select **Options > Keep in Dock**.

Sublime Text is not free, but it has an unlimited "evaluation period". Anyhow, we're going to be using it so much that even the seemingly expensive $60 price tag is worth every penny. If you can afford it, It is adviced that you [support](http://www.sublimetext.com/buy) this awesome tool. :)

Let's create a shortcut so we can launch Sublime Text from the command-line:

    $ ln -s /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl /usr/local/bin/subl

Now you can open a file with `$ subl myfile.py` or start a new project in the current directory with `$ subl .`. Pretty cool. We'll configure Sublime more in the next few sections.


# Preferences

This is an example of User Settings for a basic development but please feel free to modify or update as per your choice.

~~~
{
    "auto_complete_delay": 5,
    "auto_complete_selector": "source, text",
    "color_scheme": "Packages/User/Monokai (SL).tmTheme",
    "create_window_at_startup": false,
    "folder_exclude_patterns":
    [
        ".svn",
        ".git",
        ".DS_Store",
        "__pycache__",
        "*.pyc",
        "*.pyo",
        "*.exe",
        "*.dll",
        "*.obj",
        "*.o",
        "*.a",
        "*.lib",
        "*.so",
        "*.dylib",
        "*.ncb",
        "*.sdf",
        "*.suo",
        "*.pdb",
        "*.idb",
        "*.psd"
    ],
    "font_face": "Source Code Pro",
    "font_size": 13,
    "ignored_packages":
    [
        "Markdown",
        "Vintage"
    ],
    "open_files_in_new_window": false,
    "rulers":
    [
        80
    ],
    "translate_tabs_to_spaces": true,
    "word_wrap": true
}
~~~

# Packages

### Install Package Control
The simplest method of installation is through the Sublime Text console. The console is accessed via `View > Show Console` menu. Once open, paste the appropriate Python code for your version of Sublime Text into the console.

    import urllib.request,os,hashlib; h = '7183a2d3e96f11eeadd761d777e62404' + 'e330c659d4bb41d3bdf022e94cab3cd0'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); urllib.request.install_opener( urllib.request.build_opener( urllib.request.ProxyHandler()) ); by = urllib.request.urlopen( 'http://sublime.wbond.net/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); print('Error validating download (got %s instead of %s), please try manual install' % (dh, h)) if dh != h else open(os.path.join( ipp, pf), 'wb' ).write(by)


    # Plugins Installed

    - [Alignment](https://github.com/wbond/sublime_alignment/issues): Easy alignment of multiple selections and multi-line selections
    - [All Autocomplete](https://github.com/alienhard/SublimeAllAutocomplete): Extend Sublime Text 2 autocompletion to find matches in all open files of the current window
    - [AutoFileName](https://github.com/BoundInCode/AutoFileName): Sublime Text plugin that autocompletes filenames
    - [Bootstrap 3 Snippets](https://github.com/JasonMortonNZ/bs3-sublime-plugin): Twitter Bootstrap 3 Snippets Plugin for Sublime Text 2/3
    - [BracketHighlighter](https://github.com/facelessuser/BracketHighlighter): Bracket and tag highlighter for Sublime Text
    - [Dictionaries](https://github.com/SublimeText/Dictionaries): Hunspell UTF8 dictionaries for Sublime Text.
    - [Dictionary​Auto​Complete](https://github.com/Zinggi/DictionaryAutoComplete): This adds dictionary entries to the completions inside comments.
    - [EncodingHelper](https://github.com/SublimeText/EncodingHelper): Guess encoding of files, show in status bar, convert to UTF-8 from a variety of encodings
    - [FileDiffs](https://github.com/colinta/SublimeFileDiffs): Shows diffs between the current file, or selection(s) in the current file, and clipboard, another file, or unsaved changes.
    - [Git](https://github.com/kemayo/sublime-text-git): Plugin for some git integration into sublime text
    - [GitGutter](http://www.jisaacks.com/gitgutter): A Sublime Text 2/3 plugin to see git diff in gutter
    - [IndentXML](https://github.com/alek-sys/sublimetext_indentxml): Plugin for Sublime Text editor for reindenting XML and JSON files
    - [Jade](https://github.com/davidrios/jade-tmbundle): A comprehensive sublime text bundle for the Jade template language
    - [Jedi - Python autocompletion](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&cad=rja&uact=8&ved=0CD0QFjAC&url=https%3A%2F%2Fgithub.com%2Fsrusskih%2FSublimeJEDI&ei=OmQpU8jnGtDZoATL_YCACA&usg=AFQjCNH9VcKDU09VibnW2RNLyCjrt9SyDA&sig2=LOg29BW0rRy5BHkoObgdeQ&bvm=bv.62922401,d.cGU): Jedi is an autocompletion tool for Python that can be used in IDEs/editors.
    - [Jekyll](https://github.com/23maverick23/sublime-jekyll): A Sublime Text package for Jekyll static sites.
    - [Python Auto-Complete](https://github.com/eliquious/Python-Auto-Complete): A Sublime Text 2 plugin which adds additional auto-completion capability to Python scripts
    - [Python Imports Sorter](https://github.com/vi4m/sublime_python_imports): Sublime Text 2 plugin to organize your imports easily.
    - [Python PEP8 Autoformat](https://bitbucket.org/StephaneBunel/pythonpep8autoformat): Python PEP8 Autoformat is a Sublime Text plugin to interactively reformat Python source code according to PEP-8.
    - [PythonTraceback](https://github.com/kedder/sublime-python-traceback): Easy navigation in your python tracebacks
    - [SideBarEnhancements](https://github.com/titoBouzout/SideBarEnhancements): Enhancements to Sublime Text sidebar. Files and folders.
    - [SublimeCodeIntel](http://sublimecodeintel.github.io/SublimeCodeIntel/): Full-featured code intelligence and smart autocomplete engine
    - [SublimeLinter](http://sublimelinter.readthedocs.org/): Interactive code linting framework for Sublime Text 3
    - [SublimeLinter-pep8](https://github.com/SublimeLinter/SublimeLinter-pep8): SublimeLinter plugin for python, using pep8.
    - [TrailingSpaces](https://github.com/SublimeText/TrailingSpaces): Highlight trailing spaces and delete them in a flash.


    # SublimeLinter Settings

    ~~~
    {
        "user": {
            "debug": false,
            "delay": 0.25,
            "error_color": "D02000",
            "gutter_theme": "none",
            "gutter_theme_excludes": [],
            "lint_mode": "background",
            "linters": {
                "pep8": {
                    "@disable": false,
                    "args": [],
                    "disable": "",
                    "enable": "",
                    "excludes": [],
                    "ignore": "",
                    "max-line-length": null,
                    "rcfile": "",
                    "select": ""
                }
            },
            "mark_style": "outline",
            "no_column_highlights_line": false,
            "paths": {
                "linux": [],
                "osx": [
                    "/usr/local/bin/"
                ],
                "windows": []
            },
            "python_paths": {
                "linux": [],
                "osx": [
                    "/usr/local/bin/"
                ],
                "windows": []
            },
            "rc_search_limit": 3,
            "shell_timeout": 10,
            "show_errors_on_save": false,
            "show_marks_in_minimap": true,
            "syntax_map": {
                "html (django)": "html",
                "html (rails)": "html",
                "html 5": "html",
                "php": "html",
                "python django": "python"
            },
            "warning_color": "DDB700",
            "wrap_find": true
        }
    }
    ~~~
