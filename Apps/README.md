# Apps

Here is a quick list of apps that are generally good to use and can come in handy in day to day tasks. The apps are seperated into 4 broad categories Developer Tools, Productivity Tools, Office Apps and Others.

### Developer Tools
- [Google Chrome](https://www.google.com/intl/en/chrome/browser/): Installs Chrome which is one of the best browsers currently. All chrome browsers and extensions are saved by Google so can be synced easily.

### Diff and Merge Tools
[Kaleidoscope](http://www.kaleidoscopeapp.com/): Originally developed by "Sofa", Kaleidoscope was later acquired by "BlackPixel" - who did a great job in making it one of the best diff & merge tools for the Mac. Its beautiful user interface and great image diffing capabilities are what set it apart.

![Kaleidoscope](kaleidoscope.png)

[Beyond Compare](http://www.scootersoftware.com/): Originally a product for Microsoft Windows, the Beyond Compare team has contributed a fine diff tool to the Mac platform. Like Araxis Merge and DeltaWalker, it goes beyond (pun intended) comparing simple text and also allows diffing Word and PDF contents. In its "Pro Version", it also supports merging.

![Beyond Compare](beyond-compare.png)

**Free Alternatives**:
In case you're looking for a free alternative to the standard FileMerge app, you should also have a look at **[P4Merge](http://www.perforce.com/product/components/perforce-visual-merge-and-diff-tools)** and **[DiffMerge](http://www.sourcegear.com/diffmerge/)**.
Both can't compare in terms of features and user interface with their commercial competitors - but make for a valid alternative on Mac, Windows, and Linux.

*Thanks to Tower team*: [source](https://www.git-tower.com/blog/diff-tools-mac/)

### Productivity
- [1Password](https://agilebits.com/onepassword): Cross platform password management tool.
- [Alfred](http://www.alfredapp.com/): Replacement for spotlight. I'll mostly be upgrading to power pack.
- [AppCleaner](http://www.freemacsoft.net/appcleaner/): Uninstall Apps.
- [Caffeine](http://lightheadsw.com/caffeine/): Stops the machine from going into sleep mode.
- [Dropbox](https://www.dropbox.com/): File syncing to the cloud. It syncs files across all devices (laptop, mobile, tablet), and serves as a backup as well!
- [F.lux](https://justgetflux.com/): f.lux makes the color of your computer's display adapt to the time of day, warm at night and like sunlight during the day.
- [Google Drive](https://drive.google.com/): File syncing to the cloud too! Google Docs is a popular tool to collaborate with others.
- [Pocket](https://getpocket.com): Save For Later. Put articles, videos or pretty much anything into Pocket. Save directly from your browser or from apps like Twitter, Flipboard, Pulse and Zite.
- [SleepPillow](https://itunes.apple.com/us/app/sleep-pillow/id597419160?mt=12): Sleep Pillow plays sounds that create an ambient atmosphere for sleep enhancement at home, or for drowning out noise at work.
- [Divvy](http://mizage.com/divvy/): Don't waste time resizing and moving your windows. Divvy makes this very easy and is open source.
- [Timing](http://timingapp.com/): Keep track of the time you spend with your Mac.
- [Tomighty](http://www.tomighty.org/): A free desktop timer for the Pomodoro Technique.

### Office Apps
- [Keynote](http://www.apple.com/mac/keynote/): Create presentations on mac, this is supposed to be an alternate to PowerPoint.
- [Microsoft Office](http://www.microsoft.com/mac/buy): Microsoft Office for Mac. Includes Microsoft Word, Excel, Powerpoint and Outlook.
- [Numbers](http://www.apple.com/mac/numbers/): Create spreadsheets on mac, this is supposed to be an alternate to Excel.
- [Pages](http://www.apple.com/mac/pages/): Create text files on mac, this is supposed to be an alternate to Word.

### Others
- [Asepsis](http://asepsis.binaryage.com/): Get rid of the annoying DS_Store files. It stops them from being created anywhere on the system.
- [CheatSheet](http://www.grandtotal.biz/CheatSheet/): Tap the command key for long to see all the keyboard shortcuts of the current app.
- [Google Voice and Video](http://www.google.com/+/learnmore/hangouts/): A voice and video chat plugin. Not sure if this is required after the release of hangouts.
- [SuperDuper](http://www.shirt-pocket.com/SuperDuper/SuperDuperDescription.html): Take backups of your disk and use the backup disk to restore the machine incase of failure.
- [VLC](http://www.videolan.org/vlc/index.html): VLC Media Player. Enough said.
- [Voila](http://www.globaldelight.com/voila/): Record your screen with audio, mouse highlight and other features.
- [TimeOut](http://www.dejal.com/timeout/): Scheduled work breaks to prevent stress injuries.
