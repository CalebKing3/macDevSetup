# Credits & References

Thank you for all the awesome pages and documentation below that helped set this up.

- [Nicolashery](https://github.com/nicolashery/mac-dev-setup)
- [Gitlab Pages](https://pages.gitlab.io/)
- [Gitbook](https://github.com/GitbookIO/gitbook)
- [Sublime Plugins](https://sublime.wbond.net/)
