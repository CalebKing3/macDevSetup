## Atom

Not a fan of VIM or sublime. My favorite is **Atom**. Created by [Github](https://github.com/atom)

Go ahead and [download](https://atom.io/) it. Open the **.dmg** file, drag-and-drop in the **Applications** folder, you know the drill now. Launch the application.

Unlike Sublime, atom is a **free** text editor.

Once install go to **Atom > Settings** and select `install shell commands` then you will be able to cd to any directory and open via `atom or apm`

You can view the atom.io [packages](https://atom.io/packages) to add plugins to the editor. 
