[Mac Dev setup](git@gitlab.com:CalebKing3/macDevSetup.git)
====================

This book covers the basics of setting up a development environment on a new MacBook for most major languages. 

All instructions covered have been tested on El Capitan and Yosemite. 

Whether you are an experienced programmer or not, this book is intended for everyone to use as a reference when installing some languages/libraries.

This project was forked from [Gitbook](https://gitlab.com/pages/gitbook) on gitlab pages. 

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Create a `pages` branch: `git checkout -b origin/pages`
1. [Install Node](https://nodejs.org/en/download/) or `brew install node`
1. [Install](https://www.npmjs.com/package/gitbook) GitBook `npm install gitbook-cli -g`
1. Fetch GitBook's latest stable version `gitbook fetch latest`
1. Preview your project: `gitbook serve`
1. Add content
1. Generate the website: `gitbook build` (optional)
1. Push your changes to the pages branch: `git push origin pages`
